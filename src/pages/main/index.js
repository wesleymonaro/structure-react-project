import React, { Component } from "react";

import { Container } from "./styles";

import Title from "../../components/title";

export default class Main extends Component {
  render() {
    return (
      <Container>
        <Title text="Hello World!" />
      </Container>
    );
  }
}
